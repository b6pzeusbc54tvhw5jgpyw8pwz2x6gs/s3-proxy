. injectEnv.sh

docker stop $1

docker rm $1

docker run --name $1 -d \
	--restart always \
	--expose 80 \
	--env APP_PORT=80 \
	--env ACCESS_LOG=true \
	--env AWS_S3_BUCKET=$AWS_S3_BUCKET \
	--env AWS_REGION=$AWS_REGION \
	--env AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID \
	--env AWS_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY \
	--env VIRTUAL_HOST=$VIRTUAL_HOST \
	pottava/s3-proxy
