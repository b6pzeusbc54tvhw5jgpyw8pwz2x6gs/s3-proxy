# Usage

make `injectEnv.sh` like follow example

```
AWS_S3_BUCKET=your-bucket-name
AWS_REGION=ap-northeast-2
AWS_ACCESS_KEY_ID=YOUR_ACCESS_KEY
AWS_SECRET_ACCESS_KEY=YOUR_SECRET_KEY

# if you use https://github.com/jwilder/nginx-proxy, set VIRTUAL_HOST
VIRTUAL_HOST=some.your-domain.com
```

make injectEnv.sh excutable

```
$ chmod 500 injectEnv.sh
```

run with container name
```
$ run.sh my-s3-bucket-proxy
```
